public class Board {
    private Die die1, die2;
    private boolean[] closedTiles;

    public Board() {
        this.die1 = new Die();
        this.die2 = new Die();
        this.closedTiles = new boolean[12];
    }

    @Override
    public String toString() {
        String string = "";

        for (int i = 0; i < closedTiles.length; i++) {
            string += closedTiles[i] ? "X " : (i + 1) + " " ;
        }

        return string += "\n";
    }

    public boolean playATurn() {
        die1.roll();
        die2.roll();
        System.out.println(die1 + "\n" + die2);
        int sum = die1.getPips() + die2.getPips();
        if (!closedTiles[sum - 1]) {
            closedTiles[sum - 1] = true;
            System.out.println("Closing tile: " + sum + "\n");
            return false;
        }
        System.out.println("Tile at this position is already shut\n");
        return true;
    }
}
