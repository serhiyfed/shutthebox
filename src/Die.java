import java.util.Random;

public class Die {
    private int pips;
    private Random rng;

    public Die() {
        this.pips = 1;
        this.rng = new Random();
    }

    public int getPips() {
        return pips;
    }

    public Random getRng() {
        return rng;
    }

    public void roll() {
        //bound is exclusive, and we don't want to roll 0, so + 1
        this.pips = rng.nextInt(6) + 1;
    }

    @Override
    public String toString() {
        return "pips=" + this.pips;
    }
}
